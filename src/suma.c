#include <stdio.h>

float suma(float a, float b);

int main(void) {
	float a, b;
	scanf("%f%f", &a, &b);
	printf("La suma es: %.2f\n", suma(a, b));
	return 0;
}

float suma(float a, float b) {
	return a + b;
}

/* 
 *Cambio hecho desde la rama repo_v2
 * */
